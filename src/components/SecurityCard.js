import React, {Component} from 'react';
import axios from 'axios';
import {
    Badge,
    Button,
    Card,
    CardBody,
    CardFooter,
    CardText,
    Collapse,
    FormFeedback,
    Input,
    Label,
    Progress,
    Spinner
} from "reactstrap";
import ReactCardFlip from 'react-card-flip';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faMobileAlt,
    faGlobe,
    faEye,
    faUsers,
    faFolderPlus,
    faFolderMinus,
    faEyeSlash,
    faSpinner,
    faFireAlt,
    faHeart,
    faCaretDown
} from '@fortawesome/free-solid-svg-icons';
import {faYahoo} from '@fortawesome/free-brands-svg-icons';
import {
    CartesianGrid,
    XAxis,
    YAxis,
    Tooltip,
    ResponsiveContainer,
    AreaChart,
    Area,
    Line,
    ComposedChart
} from 'recharts';
import _ from 'lodash';
import LogoFinanzenDotNet from '../assets/finanzen.net.png';
import LogoComdirect from '../assets/comdirect.png';

class SecurityCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            portFolioActionsButton: null,
            isFlipped: false,
            isOpen: false,
            isOpenComment: false,
            cardOpen: false,
            stockChart: <div className="d-flex align-items-center justify-content-center" style={{minHeight: '200px'}}><FontAwesomeIcon icon={faSpinner} className="fa-spin" size="2x"/></div>,
            commentSaved: false,
            message: ''
        };

        this.checkSign = this.checkSign.bind(this);
        this.roundNumber = this.roundNumber.bind(this);
        this.cleanUrl = this.cleanUrl.bind(this);
        this.generateRecommendationBadge = this.generateRecommendationBadge.bind(this);
        this.onChartLoadHandler = this.onChartLoadHandler.bind(this);
        this.onMakeHotPickHandler = this.onMakeHotPickHandler.bind(this);
        this.onCommentHandler = this.onCommentHandler.bind(this);
    }

    componentDidMount() {
        switch (this.props.isPortfolio) {
            case true:
                this.setState({portFolioActionsButton: <Button color="danger" onClick={(event) => this.onRemoveFromPortfolioHandler(event)}><FontAwesomeIcon icon={faFolderMinus}/></Button>})
                break
            default:
                this.setState({portFolioActionsButton: <Button color="warning" onClick={(event) => this.onAddToPortfolioHandler(event)}><FontAwesomeIcon icon={faFolderPlus}/></Button>})
                break
        }
    }

    /* Our toggler allows to either toggle when no parameter is defined or stay as is if false is passed */
    toggle = (state) => {
        if (state === false) {
            this.setState({isOpen: state});
        } else {
            this.setState({isOpen: !this.state.isOpen})
        }
    };

    toggleComment = () => {
        this.setState({isOpenComment: !this.state.isOpenComment})
    };

    toggleCard = () => {
        this.setState({cardOpen: !this.state.cardOpen})
    };

    flip = () => {
        this.setState({
            isFlipped: !this.state.isFlipped,
            isOpenComment: false
        })
    }

    /* Check if value is negative or positive and account for custom affixes */
    checkSign = (number, affix) => {
        let customAffix = '';
        if (affix) {
            customAffix = affix
        }
        if (Math.sign(number) > 0) {
            return <span className="text-success">+{number + customAffix}</span>;
        } else if (Math.sign(number) < 0) {
            return <span className="text-danger">{number + customAffix}</span>;
        } else {
            return <span className="text-muted">{number + customAffix}</span>;
        }
    }

    /* Round number to specified digits */
    roundNumber = (number, digits) => {
        return (Math.round(number * 100) / 100).toFixed(digits);
    }

    /* Use regex to clean up URL (i.e. in order to pass it back to API when needed) */
    cleanUrl = (url) => {
        if (url !== undefined) {
            return url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "").split('/')[0];
        }
        return ''
    }

    /* Convert API response on stock recommendation to beautiful Bootstrap badges */
    generateRecommendationBadge = (recommendation) => {
        switch (recommendation) {
            case 'sell': {
                return <Badge color="danger">SELL</Badge>
            }
            case 'hold': {
                return <Badge color="warning">HOLD</Badge>
            }
            case 'buy': {
                return <Badge color="success">BUY</Badge>
            }
            case 'strong_buy': {
                return <Badge color="success">STRONG BUY</Badge>
            }
            default: {
                return <Badge color="secondary">{recommendation}</Badge>
            }
        }
    }

    /* Add a stock to users portfolio */
    onAddToPortfolioHandler = (event) => {
        event.preventDefault();
        axios.post('https://fd.okage.de/portfolios/1', {
            'symbol': this.props.dataSet.price.symbol,
        })
            .then((response) => {
                this.setState({ portFolioActionsButton: <Button color="danger" onClick={(event) => this.onRemoveFromPortfolioHandler(event)}><FontAwesomeIcon icon={faFolderMinus}/></Button>, })
            })
            .catch(error => {
                console.log(error);
            });
    }

    onRemoveFromPortfolioHandler = () => {
        axios.delete('https://fd.okage.de/portfolios/1', {
            data: {'symbol': this.props.dataSet.price.symbol},
        })
            .then((response) => {
                this.setState({ portFolioActionsButton: <Button color="warning" onClick={(event) => this.onAddToPortfolioHandler(event)}><FontAwesomeIcon icon={faFolderPlus}/></Button>})
            })
            .catch(error => {
                console.log(error);
            });
    }

    /* Add a stock to users portfolio */
    onMakeHotPickHandler = (event) => {
        event.preventDefault();
        axios.patch('https://fd.okage.de/stocks', {
            'symbol': this.props.dataSet.price.symbol,
            'hotPick': true
        })
            .then((response) => {
                console.log(response)
            })
            .catch(error => {
                console.log(error);
            });
    }

    /* Up vote stock */
    onUpVoteHandler = (event) => {
        event.preventDefault();
        axios.patch('https://fd.okage.de/stocks', {
            'symbol': this.props.dataSet.price.symbol,
            'upVote': 1
        })
            .then((response) => {
                console.log(response)
            })
            .catch(error => {
                console.log(error);
            });
    }

    onKeyDownHandler(event) {
        // auto grow and shrink text area
        event.target.style.height = 'inherit';
        event.target.style.height = `${event.target.scrollHeight}px`;
    }

    /* Add a stock to users portfolio */
    onCommentHandler = (event) => {
        axios.patch('https://fd.okage.de/stocks', {
            'symbol': this.props.dataSet.price.symbol,
            'comment': event.target.value
        })
            .then((response) => {
                this.setState({
                    message: 'Comment successfully saved!',
                    commentSaved: true
                }, () => {
                    setTimeout(() => this.setState({message: ''}), 3000 );
                })})
            .catch(error => {
                console.log(error);
                this.setState({
                    message: `An error occurred when saving your comment:<br/>${error}`,
                    commentSaved: false
                }, () => {
                    setTimeout(() => this.setState({message: ''}), 3000);
                })
            });
    }

    onChartLoadHandler = () => {
        axios.post('https://fd.okage.de/stocks/historical', {
            'symbol': this.props.dataSet.price.symbol,
        })
            .then((response) => {
                this.setState({historicalData: response.data}, () => {
                    console.log(response.data)
                    this.setState({
                            stockChart:
                                <ResponsiveContainer width="100%" height={200}>
                                    <ComposedChart
                                        width={500}
                                        height={200}
                                        data={this.state.historicalData.quotes}
                                        margin={{ top: 0, right: 45, bottom: 0, left: 0 }}>
                                        <defs>
                                            <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                                                <stop offset="5%" stopColor="#e25a97" stopOpacity={0.8}/>
                                                <stop offset="95%" stopColor="#e25a97" stopOpacity={0}/>
                                            </linearGradient>
                                        </defs>
                                        <CartesianGrid stroke="#ffffff" strokeDasharray="2" vertical={false} />
                                        <XAxis dataKey="chartDate" />
                                        <YAxis domain={['auto', 'auto']}/>
                                        <Tooltip
                                            contentStyle={{backgroundColor: '#2a1a3c', borderColor: '#2a1a3c', fontSize: '14px', boxShadow: '0 3px 6px rgba(0,0,0,0.5)'}} />
                                        <Area type="monotone" dataKey="close" stroke="#e25a97" fillOpacity={1} fill="url(#colorUv)"/>
                                        <Line type="monotone" dataKey="SMA200" stroke="#2bbb62" dot={false} strokeWidth={2}/>
                                        <Line type="monotone" dataKey="SMA50" stroke="#ff0000" dot={false} strokeWidth={2}/>
                                    </ComposedChart>
                                </ResponsiveContainer>
                        }
                    )
                })
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <ReactCardFlip isFlipped={this.state.isFlipped} flipDirection="horizontal">
                <Card className="stock-card card-shadow mb-4">
                    <CardBody className="text-left" onClick={() => {
                        this.onChartLoadHandler(this);
                        this.toggleCard()
                    }}>
                        <div className="d-flex justify-content-between">
                            <div>
                                <span className="font-weight-bold">{this.props.dataSet.price.longName}</span>
                            </div>
                            <div>
                                <img style={{'maxHeight': '48px', 'width': 'auto'}}
                                     className="img-fluid"
                                     src={"//logo.clearbit.com/" + this.cleanUrl(_.get(this.props.dataSet, 'summaryProfile.website', 'https://wtf.com'))}
                                     ref={img => this.img = img}
                                     alt={this.props.dataSet.price.longName + " Logo"}
                                     onError={ () => this.img.src = 'https://via.placeholder.com/48' }/>
                            </div>
                        </div>
                        <h2>{this.roundNumber(this.props.dataSet.price.regularMarketPrice, 2)} {this.props.dataSet.price.currency}</h2>
                        <CardText>
                            {this.checkSign(this.roundNumber(this.props.dataSet.price.regularMarketChange, 2))} {this.checkSign(this.roundNumber(this.props.dataSet.price.regularMarketChangePercent * 100, 2), '%')}<br/>
                            {this.generateRecommendationBadge(_.get(this.props.dataSet, 'financialData.recommendationKey', 'n/a'))} <FontAwesomeIcon
                            className="fa-fw" icon={faUsers}/> {_.get(this.props.dataSet, 'financialData.numberOfAnalystOpinions', 'n/a')}
                        </CardText>
                    </CardBody>

                    <Collapse isOpen={this.state.cardOpen}>
                        <CardBody className="p-0 text-center">
                            {this.state.stockChart}
                        </CardBody>

                        <CardBody>
                            <table className="table table-sm text-monospace">
                                <tbody>
                                <tr className="text-monospace">
                                    <th scope="row">Exchange</th>
                                    <td className="w-50">{this.props.dataSet.price.exchangeName}</td>
                                </tr>
                                <tr className="text-monospace">
                                    <th scope="row">Symbol</th>
                                    <td className="w-50">{this.props.dataSet.price.symbol} <a
                                        target="_blank"
                                        className="text-white text-success"
                                        href={'http://de.finance.yahoo.com/q?s=' + this.props.dataSet.price.symbol}>
                                        <FontAwesomeIcon icon={faYahoo}/>
                                    </a>
                                    </td>
                                </tr>
                                <tr className="text-monospace">
                                    <th scope="row">WKN</th>
                                    <td className="w-50">
                                        {this.props.dataSet.price.wkn ?
                                            <>
                                                {this.props.dataSet.price.wkn}
                                            </>
                                        : 'n/a'}
                                    </td>
                                </tr>
                                <tr className="text-monospace">
                                    <th scope="row">ISIN</th>
                                    <td className="w-50">
                                        {this.props.dataSet.price.isin ?
                                            <>
                                            {this.props.dataSet.price.isin}
                                                <a
                                                    target="_blank"
                                                    className="ml-2 text-white"
                                                    href={'http://www.finanzen.net/suchergebnis.asp?frmAktiensucheTextfeld=' + this.props.dataSet.price.isin}>
                                            <img src={LogoFinanzenDotNet} style={{width: '16px'}} />
                                            </a>
                                            <a
                                                target="_blank"
                                                className="ml-2 text-white"
                                                href={'https://www.comdirect.de/inf/aktien/' + this.props.dataSet.price.isin}>
                                            <img src={LogoComdirect} style={{width: '16px'}} />
                                            </a>
                                            </>
                                        : 'n/a'}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        RSL(27)
                                    </th>
                                    <td className="w-50">
                                        {_.get(this.state.historicalData, 'rsl27', <Spinner size="sm" color="danger"/>)}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Debt/Equity
                                    </th>
                                    <td className="w-50">
                                        <Progress
                                            color={_.get(this.props.dataSet, 'financialData.debtToEquity', 'n/a') >= 100 ? 'danger' : 'success'}
                                            value={_.get(this.props.dataSet, 'financialData.debtToEquity', 'n/a')}
                                            max="200">
                                                {_.get(this.props.dataSet, 'financialData.debtToEquity', 'n/a')}
                                        </Progress>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Return on Equity
                                    </th>
                                    <td>
                                        {_.get(this.props.dataSet, 'financialData.returnOnEquity', 'n/a')}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Revenue/Share
                                    </th>
                                    <td>
                                        {_.get(this.props.dataSet, 'financialData.revenuePerShare', 'n/a')}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Profit Margin
                                    </th>
                                    <td>
                                        {_.get(this.props.dataSet, 'financialData.profitMargins', 'n/a')}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Current Ratio
                                    </th>
                                    <td>
                                        <Progress
                                            color={_.get(this.props.dataSet, 'financialData.currentRatio', 'n/a') >= 1.2 ? 'success' : 'danger'}
                                            value={_.get(this.props.dataSet, 'financialData.currentRatio', 'n/a')}
                                            max="3">
                                            {_.get(this.props.dataSet, 'financialData.currentRatio', 'n/a')}
                                        </Progress>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Quick Ratio
                                    </th>
                                    <td>
                                        <Progress
                                            color={_.get(this.props.dataSet, 'financialData.quickRatio', 'n/a') >= 1 ? 'success' : 'danger'}
                                            value={_.get(this.props.dataSet, 'financialData.quickRatio', 'n/a')}
                                            max="2">
                                            {_.get(this.props.dataSet, 'financialData.quickRatio', 'n/a')}
                                        </Progress>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            {!_.isNil(this.props.peers) ? this.props.peers.map((peer, index) => {
                                return <Badge className="mr-2" color="info" key={index}>{peer}</Badge>
                            }) : ''}
                        </CardBody>
                        <CardFooter className="d-flex justify-content-between">
                            {this.state.portFolioActionsButton}
                            <Button color="red" onClick={this.onMakeHotPickHandler}><FontAwesomeIcon
                                icon={faFireAlt}/></Button>
                            <Button color="blue" onClick={this.onUpVoteHandler}><FontAwesomeIcon
                                icon={faHeart}/></Button>
                            <Button color="success" onClick={() => this.flip(!this.state.isFlipped)}><FontAwesomeIcon
                                icon={faEye}/></Button>
                        </CardFooter>
                    </Collapse>
                    <Progress
                        color={_.get(this.props.dataSet, 'price.upVotes', 0) >= 5 ? 'danger' : 'warning'}
                        value={_.get(this.props.dataSet, 'price.upVotes', 0)}
                        max="10"
                        style={{height: '.25rem'}}>
                        {/* {_.get(this.props.dataSet, 'price.upVotes', 0)} */}
                    </Progress>
                </Card>

                <Card className="stock-card card-shadow mb-4">
                    <CardBody className="text-left">
                        <div className="d-flex justify-content-between">
                            <div>
                                <span className="font-weight-bold">{this.props.dataSet.price.longName}</span><br/>
                                <span>({this.props.dataSet.price.exchangeName}:{this.props.dataSet.price.symbol})</span>
                            </div>
                            <div>
                                <img style={{'maxHeight': '48px', 'width': 'auto'}}
                                     className="img-fluid"
                                     src={"//logo.clearbit.com/" + this.cleanUrl(_.get(this.props.dataSet, 'summaryProfile.website', 'https://wtf.com'))}
                                     ref={img => this.img = img}
                                     alt={this.props.dataSet.price.longName + " Logo"}
                                     onError={ () => this.img.src = 'https://via.placeholder.com/48' }/>
                            </div>
                        </div>
                        <div className="mt-2">
                            <h6 className="font-weight-bold">Origin</h6>
                            <p>{_.get(this.props.dataSet, 'summaryProfile.country', 'n/a')}</p>
                            <h6 className="font-weight-bold">Industry</h6>
                            <p>{_.get(this.props.dataSet, 'summaryProfile.industry', 'n/a')}, {_.get(this.props.dataSet, 'summaryProfile.sector', 'n/a')}</p>
                            <h6 className="font-weight-bold" style={{cursor: 'pointer'}} onClick={this.toggle}>Description <FontAwesomeIcon className="fa-fw" icon={faCaretDown}/></h6>
                            <Collapse isOpen={this.state.isOpen}>
                                <p>{_.get(this.props.dataSet, 'summaryProfile.longBusinessSummary', 'n/a')}</p>
                            </Collapse>
                            <Label onClick={this.toggleComment} className="font-weight-bold" for="comment">Comment <FontAwesomeIcon className="fa-fw" icon={faCaretDown}/></Label>
                            <Collapse isOpen={this.state.isOpenComment}>
                                <Input onKeyDown={(event) => this.onKeyDownHandler(event)} onBlur={this.onCommentHandler} defaultValue={this.props.dataSet.price.comment} valid={this.state.commentSaved} type="textarea" name="comment" id="comment" style={{minHeight: '100px'}}/>
                                <FormFeedback className="mb-3" valid={this.state.commentSaved}>{this.state.message}</FormFeedback>
                            </Collapse>
                            <h6 className="font-weight-bold">Contact</h6>
                            <FontAwesomeIcon className="fa-fw" icon={faGlobe}/> <a target="_blank" style={{color: '#e25a97'}} href={_.get(this.props.dataSet, 'summaryProfile.website', 'n/a')}>{_.get(this.props.dataSet, 'summaryProfile.website', 'n/a')}</a><br/>
                            <FontAwesomeIcon className="fa-fw" icon={faMobileAlt}/> <a style={{color: '#e25a97'}} href={"tel:" + _.get(this.props.dataSet, 'summaryProfile.phone', 'n/a')}>{_.get(this.props.dataSet, 'summaryProfile.phone', 'n/a')}</a>
                        </div>
                    </CardBody>
                    <CardFooter className="text-right">
                        <Button color="success" onClick={() => {
                            this.toggle(false);
                            this.flip(!this.state.isFlipped);
                        }}><FontAwesomeIcon icon={faEyeSlash}/></Button>
                    </CardFooter>
                </Card>
            </ReactCardFlip>
        );
    }
}

export default SecurityCard;
