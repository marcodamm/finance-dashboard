import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {Container, Form, Button, Row, Col, Input, InputGroup, FormGroup, Label, Alert} from 'reactstrap';
import {useDispatch, useSelector} from "react-redux";
import {login} from "../actions/userActions";

const Login = ({location, history}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const dispatch = useDispatch()
    const userLogin = useSelector(state => state.userLogin)
    const {loading, error, userInfo} = userLogin
    const redirect = location.search ? location.search.split('=')[1] : '/'

    useEffect(() => {
        if(userInfo) {
            history.push(redirect)
        }
    }, [history, userInfo, redirect])

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(login(email, password))
    }

    return <Container>
            <Row>
                <Col>
                    <h1>Sign In</h1>
                    {error && <Alert color="danger">{error}</Alert>}
                    {loading && 'Loading...'}
                    <Form onSubmit={submitHandler}>
                        <FormGroup id="email">
                            <Label>E-Mail Address</Label>
                            <Input placeholder="mail@example.com" onChange={(e) => setEmail(e.target.value)} value={email}/>
                        </FormGroup>

                        <FormGroup id="password">
                            <Label>Password</Label>
                            <Input placeholder="A secure password" onChange={(e) => setPassword(e.target.value)} value={password}/>
                        </FormGroup>

                        <Button type="submit" color="primary">Sign In</Button>
                    </Form>
                </Col>
                <Row>
                    <Col>
                        New Customer? <Link to={redirect ? `/register?redirect=${redirect}` : '/register'}>Register</Link>
                    </Col>
                </Row>
            </Row>
        </Container>
}

export default Login
