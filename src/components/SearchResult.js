import React from 'react'

const SearchResult = (props) => {
    return(
        <div onClick={props.loadSearchResult(props.symbol)} className="truncated border-bottom p-3">
            <span className="font-weight-bold">{props.name}</span> ({props.symbol})
        </div>
    )
}

export default SearchResult;
