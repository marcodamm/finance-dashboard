import React, {Component} from 'react';
import axios from 'axios';
import {connect} from "react-redux";
import {
    Button,
    InputGroup,
    InputGroupAddon,
    Input,
    Nav,
    NavItem,
    NavLink,
    CardDeck,
    Navbar, Label, Row, Col, Container, FormText, Spinner,
} from 'reactstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faDiceFive,
    faFireAlt,
    faSearchDollar,
    faFolderOpen,
    faTimes,
    faCog
} from '@fortawesome/free-solid-svg-icons'
import SecurityCard from "./SecurityCard";
import SearchResult from "./SearchResult";

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuRandomPicksClassName: '',
            menuPortfolioClassName: 'active',
            menuHotPicksClassName: '',
            randomPicks: null,
            stockData: null,
            portfolios: [],
            hotPicks: null,
            symbol: '',
            searchResults: '',
            searchResultsShow: false,
            settingsShow: false,
            dropdownOpen: false,
            loading: true,
            settings: {
                sort: 'symbol'
            }
        };

        this.onChangeSymbolHandler = this.onChangeSymbolHandler.bind(this);
        this.onSubmitSymbolHandler = this.onSubmitSymbolHandler.bind(this);
        this.onGetHotPicksHandler = this.onGetHotPicksHandler.bind(this);
        this.getRandomStockHandler = this.getRandomStockHandler.bind(this);
        this.showSearchResults = this.showSearchResults.bind(this);
        this.showSettings = this.showSettings.bind(this);
        this.loadSearchResult = this.loadSearchResult.bind(this);
        this.updateSettings = this.updateSettings.bind(this);
    }

    showSettings = () => { this.setState({settingsShow: !this.state.settingsShow}) };
    showSearchResults = () => { this.setState({searchResultsShow: true}) };
    loadSearchResult = symbol => {
        return () => {
            this.setState({symbol: symbol}, () => {
                this.onSubmitSymbolHandler()
            })
        }
    }

    clearSearchResults = () => {
        this.setState({
            searchResultsShow: false,
            symbol: ''
        })
    };

    updateSettings(event) {
        let updatedValue = event.target.value;
        let updatedSettings = {...this.state.settings}
        updatedSettings.sort = updatedValue;
        this.setState({settings: updatedSettings}, () => {
            localStorage.setItem('settings', this.state.settings.sort);
        })
    }

    onChangeSymbolHandler(event) {
        let symbol = event.target.value.toUpperCase();
        this.setState({
            symbol: symbol,
        }, () => {
            axios.post('https://fd.okage.de/stocks/fuzzy', {
                symbol: this.state.symbol
            })
                .then((response) => {
                    this.setState({
                        searchResults: response.data,
                    })
                })
                .catch(error => {
                    console.log(error);
                })
        })
        this.showSearchResults();
    }

    onGetHotPicksHandler(event) {
        event.preventDefault();
        this.setState({
            loading: true,
            settingsShow: false,
            searchResultsShow: false,
        })
        axios.get('https://fd.okage.de/stocks/hotpicks', {
            params: {
                sort: localStorage.getItem('settings')
            }})
            .then((response) => {
                this.setState({
                    loading: false,
                    hotPicks: response.data,
                    stockData: null,
                    historicalData: null,
                    portfolios: null,
                    randomPicks: null
                })
            })
            .catch(error => {
                console.log(error);
            })
    }

    onSubmitSymbolHandler() {
        let symbol = this.state.symbol;
        axios.all([
            axios({
                method: 'post',
                url: 'https://fd.okage.de/stocks/quote/',
                data: {
                    symbol: symbol
                }
            }),
            axios({
                method: 'post',
                url: 'https://fd.okage.de/stocks/historical/',
                data: {
                    symbol: symbol
                }
            })
        ])
            .then(axios.spread((quotes, historical) => {
                this.setState({
                    settingsShow: false,
                    stockData: quotes.data,
                    historicalData: historical.data,
                    symbol: '',
                    hotPicks: null,
                    portfolios: null,
                    randomPicks: null,
                    searchResultsShow: false
                })
            }))
            .catch(error => {
                console.log(error);
            })
    }

    getRandomStockHandler() {
        this.setState({
            loading: true,
            settingsShow: false,
            searchResultsShow: false,
        })
        axios.get('https://fd.okage.de/stocks/quote/random')
            .then((response) => {
                this.setState({
                    loading: false,
                    randomPicks: response.data,
                    stockData: null,
                    historicalData: null,
                    portfolios: null,
                    hotPicks: null
                })
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log('Error', error.message);
                }
                console.log(error.config);
            });
    }

    onGetPortfolioHandler() {
        this.setState({
            loading: true,
            settingsShow: false,
            searchResultsShow: false,
        })
        axios.get('https://fd.okage.de/portfolios/1', {
            params: {
                sort: localStorage.getItem('settings')
            }})
            .then(response => {
                this.setState({
                    loading: false,
                    portfolios: response.data,
                    hotPicks: null,
                    stockData: null,
                    historicalData: null,
                    randomPicks: null
                });
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log('Error', error.message);
                }
                console.log(error.config);
            });
    }

    componentDidMount() {
        this.setState({settings: localStorage.getItem('settings') || ''});
        axios.get('https://fd.okage.de/portfolios/1')
            .then(response => {
                this.setState({
                    loading: false,
                    portfolios: response.data
                });
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log('Error', error.message);
                }
                console.log(error.config);
            });
    }

    render() {

        let cards = null;
        let hotPicks = null;
        let stockData = null;
        let randomPicks = null;
        let searchResults = '';
        let loadingIndicator = (
            <div className="d-flex justify-content-center align-items-center position-fixed h-100 w-100 m-0 p-0 pb-5 overlay--loading">
                <Spinner color="light" style={{width: '3rem', height: '3rem'}}/>
            </div>
        );

        if (this.state.searchResults) {
            searchResults = this.state.searchResults.map((item, index) => {
                    return (
                        <SearchResult
                            key={index}
                            loadSearchResult={this.loadSearchResult}
                            name={item.name}
                            symbol={item.symbol}/>
                    )
                }
            )
        }

        if (this.state.portfolios) {
            cards = this.state.portfolios.map((item, index) => {
                return (
                    <SecurityCard
                        key={index}
                        isPortfolio={true}
                        dataSet={item.quotes}/>
                )
            })
        }

        if (this.state.randomPicks) {
            randomPicks = this.state.randomPicks.map((randomPick, index) => {
                return (
                    <SecurityCard
                        key={index}
                        id={index}
                        dataSet={randomPick.quotes}/>
                )
            })
        }

        if (this.state.hotPicks) {
            hotPicks = this.state.hotPicks.map((hotPick, index) => {
                return (
                    <SecurityCard
                        key={index}
                        id={index}
                        dataSet={hotPick.quotes}/>
                )
            })
        }

        if (this.state.stockData) {
            stockData =
                <SecurityCard
                    key="1"
                    dataSet={this.state.stockData.quotes}
                    peers={this.state.stockData.peers}
                    historicalData={this.state.historicalData.quotes}/>
        }

        if (!this.state.loading) {
            loadingIndicator = '';
        }

        const {userInfo} = this.props.userLogin

        return (
            <>
                <div className={this.state.settingsShow ?
                    'd-block position-fixed bg-white h-100 w-100 m-0 p-0 pb-5 overlay--search' :
                    'd-none position-fixed bg-white h-100 w-100 m-0 p-0 pb-5 overlay--search'}>
                    <div className="bg-primary p-2 border-bottom overlay--header">
                        <h5 className="text-white p-0 m-0 modal-title">Settings</h5>
                        <span className="close text-white" onClick={() => {
                            this.showSettings();
                        }}><FontAwesomeIcon icon={faTimes}/></span>
                    </div>
                    <Container>
                        <Row className="mt-3">
                            <Col>
                                <Label className="text-dark" for="sortSettings">Sort securities by:</Label>
                                <Input onChange={(event) => this.updateSettings(event)} type="select" name="sortSettings" id="sortSettings" className="text-dark bg-light">
                                    <option value="longName">Company name</option>
                                    <option value="regularMarketPrice">Price</option>
                                    <option value="regularMarketChangePercent">Relative change</option>
                                    <option value="symbol">Symbol</option>
                                    <option value="upVotes">Upvotes</option>
                                </Input>
                                <FormText>Settings are automatically saved.</FormText>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <div className={this.state.searchResultsShow ?
                    'd-block position-fixed bg-white h-100 w-100 m-0 p-0 pb-5 overlay--search' :
                    'd-none position-fixed bg-white h-100 w-100 m-0 p-0 pb-5 overlay--search'}>
                    <div className="bg-primary p-2 border-bottom overlay--header">
                        <h5 className="text-white p-0 m-0 modal-title">Top 10 results</h5>
                        <span className="close text-white" onClick={() => {
                            this.clearSearchResults();
                        }}><FontAwesomeIcon icon={faTimes}/></span>
                    </div>
                    {searchResults}
                </div>
                <CardDeck>
                    {loadingIndicator}
                    {stockData}
                    {hotPicks}
                    {cards}
                    {randomPicks}
                </CardDeck>
                <Navbar dark color="primary" fixed="bottom" className="px-2">
                    <Nav className="d-flex scroll-wrap align-items-center">
                        <div className="fade-out"></div>
                        <NavItem className="mr-2">
                            <InputGroup>
                                <Input placeholder="Symbol or company name" onChange={this.onChangeSymbolHandler}
                                       value={this.state.symbol}/>
                                <InputGroupAddon addonType="append"><Button onClick={this.onSubmitSymbolHandler}
                                                                            color="warning"
                                                                            className="border-0"><FontAwesomeIcon
                                    icon={faSearchDollar} size="lg"/></Button></InputGroupAddon>
                            </InputGroup>
                        </NavItem>
                        <NavItem className="mx-2">
                            <NavLink className="p-1 btn btn-primary text-center p-0 text-white" onClick={this.onGetHotPicksHandler}>
                                <FontAwesomeIcon icon={faFireAlt} size="lg"/>
                                <p className="m-0 small">Hot Picks</p>
                            </NavLink>
                        </NavItem>
                        <NavItem className="mx-2">
                            <NavLink className="p-1 btn btn-primary text-center p-0 text-white"
                                     onClick={this.onGetPortfolioHandler.bind(this)}>
                                <FontAwesomeIcon icon={faFolderOpen} size="lg"/>
                                <p className="m-0 small">Portfolio</p>
                            </NavLink>
                        </NavItem>
                        <NavItem className="mx-2">
                            <NavLink className="p-1 btn btn-primary text-center p-0 text-white"
                                     onClick={this.getRandomStockHandler.bind(this)}>
                                <FontAwesomeIcon icon={faDiceFive} size="lg"/>
                                <p className="m-0 small">Random</p>
                            </NavLink>
                        </NavItem>
                        <NavItem className="mx-2 pr-5">
                            <NavLink className="p-1 btn btn-primary text-center p-0 text-white" onClick={this.showSettings}>
                                <FontAwesomeIcon icon={faCog} size="lg"/>
                                <p className="m-0 small">Settings</p>
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Navbar>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        userLogin: state.userLogin
    }
};

export default connect(
    mapStateToProps
)(Dashboard);
