import React from 'react';
import {BrowserRouter as Router, Route } from "react-router-dom";
import Dashboard from "./components/Dashboard";
import {Container, Row, Col} from "reactstrap";
import Login from "./components/Login";

function App() {
  return (
      <Router basename={window.location.pathname || ''}>
          <Container fluid className="App my-3">
              <Row className="pb-5">
                  <Col>
                      <Route exact path='/' component={Dashboard} />
                      <Route path='/login' component={Login} />
                  </Col>
              </Row>
          </Container>
      </Router>
  );
}

export default App;
